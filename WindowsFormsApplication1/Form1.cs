﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
            timer1.Stop();
            label2.Text = "Welcome!\nIf its your 1st time, please check 'Help'";
            label2.BackColor = picBoxMap.BackColor;
            label2.Visible = true;
        }
        int speedLeft = 4;  //ball move
        int speedTop = 4;
        int timerTick = 0;
        int Player1score = 0;
        int Player2score = 0;
        int randomX, randomY;   //variables for randomization the blocks position;
        bool stering;

        int positionHost;
        int positionClient;
        string hostAddress = GetLocalIPAddress();
        int hostPort=100;
        // enum typeConnecttion {notSelected, HostSelected, ClientSelected };   //0-host, 1-client
        short typeConnecttion=0; //0-not selected, 1-host, 2-client
        IPEndPoint zdalnyIP = new IPEndPoint(IPAddress.Any, 0);

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (typeConnecttion == 1)
            {
                try
                {
                    UdpClient serwer = new UdpClient(hostPort);
                    Byte[] odczyt = serwer.Receive(ref zdalnyIP);
                    string dane = Encoding.ASCII.GetString(odczyt);
                    //zapisanie odebranych danych;
                    listBox1.Items.Add(dane.ToString());
                    serwer.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString(), "Błąd");
                }
            }
            else if (typeConnecttion == 2)
            {
                try
                {
                    UdpClient klient = new UdpClient(hostAddress, hostPort);
                    Byte[] dane = Encoding.ASCII.GetBytes(textBox1.Text);
                    klient.Send(dane, dane.Length);
                    listBox1.Items.Add("send... ");
                    klient.Close();
                }
                catch (Exception ex)
                {
                    listBox1.Items.Add("Błąd nie wyslano!");
                    MessageBox.Show(ex.ToString(), "Błąd");
                }
            }
            
              label1.Text = timerTick.ToString();
              timerTick += 1;
              PicBoxBall.Left += speedLeft;
              PicBoxBall.Top += speedTop;
            //set the playes position
            
              picBoxPlayer1.Top = Cursor.Position.Y;
              //collision with player 1
              if(PicBoxBall.Bottom >= picBoxPlayer1.Top && PicBoxBall.Top <= picBoxPlayer1.Bottom && PicBoxBall.Left <= picBoxPlayer1.Right && PicBoxBall.Right >= picBoxPlayer1.Left)
              {                       
                  speedLeft += 1;
                  speedTop += 1;
                  speedLeft = -speedLeft;
                  Player1score += 5;
                  lblPlayer1Score.Text = Player1score.ToString();
              }
              //collision with player 2
              else if (PicBoxBall.Bottom >= picBoxPlayer2.Top && PicBoxBall.Top <= picBoxPlayer2.Bottom && PicBoxBall.Right >= picBoxPlayer2.Left && PicBoxBall.Right <= picBoxPlayer2.Right)
              {
                  speedLeft += 1;
                  speedTop += 1;
                  speedLeft = -speedLeft;
                  Player2score += 5;
                  lblPlayer2Score.Text = Player2score.ToString();
              }
               //collision with walls
              if (PicBoxBall.Top <= picBoxMap.Top)
                  speedTop = -speedTop;
              if (PicBoxBall.Bottom >= picBoxMap.Bottom)
                  speedTop = -speedTop;
                  
        }

        public void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {   //making new window from strip menu 
            label2.Visible = false;

            Form StartWindow = new Form();
            StartWindow.Show();
            StartWindow.Width = 350;
            StartWindow.Height = 280;
            StartWindow.Name = "New Game";

            TextBox txtBoxPlayerName = new TextBox();
            StartWindow.Controls.Add(txtBoxPlayerName);
            txtBoxPlayerName.Location = new Point(19, 60);
            txtBoxPlayerName.Text = "Name of player";

            ComboBox cmbBoxGameMode = new ComboBox();
            StartWindow.Controls.Add(cmbBoxGameMode);
            cmbBoxGameMode.Location = new Point(19, 84);
            cmbBoxGameMode.Text = "Game Type";
            cmbBoxGameMode.Items.Add("Host");
            cmbBoxGameMode.Items.Add("Client");
            cmbBoxGameMode.Width = 100;
           
            TextBox txtBoxIpNubmer = new TextBox();
            StartWindow.Controls.Add(txtBoxIpNubmer);
            txtBoxIpNubmer.Location = new Point(19, 140);
            txtBoxIpNubmer.Text = "0.0.0.0";
            txtBoxIpNubmer.Visible = false;

            NumericUpDown numUDPort = new NumericUpDown();
            StartWindow.Controls.Add(numUDPort);
            numUDPort.Location = new Point(19, 180);
            numUDPort.Value = 0000;
            numUDPort.Visible = false;
            numUDPort.Width = 100;
            numUDPort.Value = 100;

            Label lblIpNumber = new Label();
            StartWindow.Controls.Add(lblIpNumber);
            lblIpNumber.Location = new Point(19, 125);
            lblIpNumber.Text = "Host IP";
            lblIpNumber.Visible = false;

            Label lblPort = new Label();
            StartWindow.Controls.Add(lblPort);
            lblPort.Location = new Point(19, 165);
            lblPort.Text = "Port";
            lblPort.Visible = false;

            Button btnRunHost = new Button();
            StartWindow.Controls.Add(btnRunHost);
            btnRunHost.Location = new Point(19, 140);
            btnRunHost.Text = "Run Host";
            btnRunHost.Visible = false;
            btnRunHost.Click += delegate
             {
                 //host test
             };

            Label lblIpChecked = new Label();
            StartWindow.Controls.Add(lblIpChecked);
            lblIpChecked.Location = new Point(19, 125);
            lblIpChecked.Text = "Your IP is : 1.1.1.1";
            lblIpChecked.Visible = false;

            Label lblDialogInfo = new Label();
            StartWindow.Controls.Add(lblDialogInfo);
            lblDialogInfo.Location = new Point(19, 210);
            lblDialogInfo.Text = "";
            lblDialogInfo.Width = 250;

            Label lblConnectionStatus = new Label();
            StartWindow.Controls.Add(lblConnectionStatus);
            lblDialogInfo.Location = new Point(10, 10);
            lblDialogInfo.Text = "no connect";

            Button btnJoin = new Button();
            StartWindow.Controls.Add(btnJoin);
            btnJoin.Text = "Join";
            btnJoin.Location = new Point(140, 140);
            btnJoin.Width = 50;
            btnJoin.Visible = false;
            btnJoin.Click += delegate
             {
                 //join test
             };

            Button btnStartGame = new Button();
            StartWindow.Controls.Add(btnStartGame);
            btnStartGame.Text = "Start";
            btnStartGame.Location = new Point(207, 94);
            btnStartGame.Click += delegate
            {
                timer1.Start();
                lblPlayer1Name.Text = txtBoxPlayerName.Text;
                StartWindow.Close();
                //Cursor.Hide();
            };

            Button btnApply = new Button();
            StartWindow.Controls.Add(btnApply);
            btnApply.Text = "Apply";
            btnApply.Location = new Point(140,83);
            btnApply.Width = 50;
            btnApply.Click += delegate
            {
                if(cmbBoxGameMode.SelectedIndex==1)
                {
                    txtBoxIpNubmer.Visible = true;
                    lblIpNumber.Visible = true;
                    btnRunHost.Visible = false;
                    lblIpChecked.Visible = false;
                    lblDialogInfo.Text = "You are Client. Please set IP of Host.";
                    btnJoin.Visible = true;
                    //hostPort = System.Convert.ToInt16(numUDPort.Value);
                    numUDPort.Visible = true;
                    lblPort.Visible = true;
                    typeConnecttion = 2;
                    textBox1.Text = "Type text here to test connection.";
                }
                else if(cmbBoxGameMode.SelectedIndex==0)
                {
                    txtBoxIpNubmer.Visible = false;
                    lblIpNumber.Visible = false;
                    btnRunHost.Visible = true;
                    lblIpChecked.Visible = true;
                    lblIpChecked.Text = hostAddress;
                    lblDialogInfo.Text = "You are Host. Wait for join the Client.";
                    btnJoin.Visible = false;
                   // hostPort = System.Convert.ToInt16(numUDPort.Value);
                    numUDPort.Visible = true;
                    lblPort.Visible = true;
                    typeConnecttion = 1;
                    textBox1.Text = "Look for test message.";
                }
            };

            Button btnClear = new Button();
            StartWindow.Controls.Add(btnClear);
            btnClear.Text = "Clear";
            btnClear.Location = new Point(207, 124);
            btnClear.Click += delegate
             {
                 txtBoxPlayerName.Text = "";
                 cmbBoxGameMode.SelectedItem = 0;
                 txtBoxIpNubmer.Visible = false;
                 lblDialogInfo.Text = "";
                 lblIpChecked.Visible = false;
                 lblIpNumber.Visible = false;
                 btnRunHost.Visible = false;
                 btnJoin.Visible = false;
                 numUDPort.Visible = false;
                 lblPort.Visible = false;
             };
            
            
        }

        public static string GetLocalIPAddress()
        {
            var hostIP = Dns.GetHostEntry(Dns.GetHostName());
            foreach(var ip in hostIP.AddressList)
            {
                if(ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP address not Found!");
        }
        public void hostOperations ()
        {
            try
            {
                UdpClient serwer = new UdpClient(hostPort);
                Byte[] odczyt = serwer.Receive(ref zdalnyIP);
                string dane = Encoding.ASCII.GetString(odczyt);
                //zapisanie odebranych danych;
                serwer.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd");
            }
        }
        public void ClientOperations()
        {
            try
            {
                UdpClient klient = new UdpClient(hostAddress, hostPort);
                Byte[] dane = Encoding.ASCII.GetBytes(positionHost.ToString());
                klient.Send(dane, dane.Length);
                klient.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Błąd");
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Do you really want to exit?", "Nooooooo !", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
                Close();  
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label2.Visible = false;
            DialogResult dialogResult = MessageBox.Show("Ping Pong Multiplayer v.0.1\nCreated by Dawid Ziółkowski\n-Gut Programmer ;)\n\n'A true master never stop learning'", "About", MessageBoxButtons.OK);
        }

        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            label2.Visible = false;
            DialogResult dialogResult = MessageBox.Show("This is multiplayer game using the UDP protocol.\nHost and Client Connection Was testing at only one Laptop and at one IP.\n\n-For test the connection you can use 'console' at the bottom.\n\n-To start, please click 'Game' in strip menu and choose 'New Game' to set\ngame type (connection type - host/client)\n\nThis app is in the testing phase so Sorry for any bugs and lags.", "About", MessageBoxButtons.OK);
        }

        private void dragPlayer(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
                this.picBoxPlayer2.Top -= 10;
            else if (e.KeyCode == Keys.Down)
                this.picBoxPlayer2.Top += 10;
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label2.Visible = false;

            Form settingsWindow = new Form();
            settingsWindow.Show();
            settingsWindow.Width = 350;
            settingsWindow.Height = 280;
            settingsWindow.Name = "Settings";

            CheckBox chBoxFullScreen = new CheckBox();
            settingsWindow.Controls.Add(chBoxFullScreen);
            chBoxFullScreen.Location = new Point(19, 60);
            chBoxFullScreen.Text = "FullScreen";

            CheckBox chBoxMouseHide = new CheckBox();
            settingsWindow.Controls.Add(chBoxMouseHide);
            chBoxMouseHide.Location = new Point(19, 90);
            chBoxMouseHide.Text = "Hide cursor";

            CheckBox chBoxSterringMouse = new CheckBox();
            settingsWindow.Controls.Add(chBoxSterringMouse);
            chBoxSterringMouse.Location = new Point(19, 120);
            chBoxSterringMouse.Text = "Mouse";

            CheckBox chBoxSterringKeyboard = new CheckBox();
            settingsWindow.Controls.Add(chBoxSterringKeyboard);
            chBoxSterringKeyboard.Location = new Point(39, 120);
            chBoxSterringKeyboard.Text = "Keyboard";

            Button btnOK = new Button();
            settingsWindow.Controls.Add(btnOK);
            btnOK.Location = new Point(19, 150);
            btnOK.Text = "OK";
            btnOK.Width = 30;
            btnOK.Click += delegate
             {
                 if (chBoxFullScreen.Checked)
                 {
                    //set full screen
                }
                 else
                 {
                    //set window screen
                }
                 if (chBoxMouseHide.Checked)
                 {
                    //set mouse hide
                }
                 else
                 {
                    //set mouse show
                }
                 if(chBoxSterringMouse.Checked)
                 {
                     stering = true;
                 }
                 else if(chBoxSterringKeyboard.Checked)
                 {
                     stering = false;
                 }
                 settingsWindow.Close();
             };

        }

       /* private void Form1_Load(object sender, EventArgs e)
        {

        }*/
    }
}
